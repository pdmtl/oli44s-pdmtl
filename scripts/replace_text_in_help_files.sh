#bash
#changes portions of text ONLY in the help files
#cannot change portions of text that have { } characters (this can be solved)

if [ $# -ne 3 ]
then 
echo "Usage : $0 pdmtl_foler old_text new_text"
     exit 1
fi

echo "Checking for the pdmtl folder"
if [ -f $1/0.info.pd ]; then
    echo "Found 0.info.pd, continuing..."
else
    echo "ERROR: Did not find 0.info.pd"
    exit 1
fi

echo "--- DANGER ZONE ---"


echo "Replacing all references to $1 with $2"

perl -pi -e "s{$2}{$3}g" `find $1 -name '*-help.pd'`

echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
