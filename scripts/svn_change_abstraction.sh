#bash
############################
# WARNING: This script is dangereous!
# If you make a mistake, simply make a 'svn revert'
# But it does not always work!!!
############################



if [ $# -ne 2 ]
then 
echo "Usage : $0 old_abstraction_name(wihout the .pd) new_abstraction_name(wihout the .pd)"
     exit 1
fi


echo "--- DANGER ZONE ---"


HELP_FILE_OLD=$1-help.pd
HELP_FILE_NEW=$2-help.pd
FILE_OLD=$1.pd
FILE_NEW=$2.pd

echo "Moving $FILE_OLD to $FILE_NEW"
svn mv $FILE_OLD $FILE_NEW

echo "Moving $HELP_FILE_OLD to $HELP_FILE_NEW"
svn mv $HELP_FILE_OLD $HELP_FILE_NEW



echo "----"


echo "Changing references from $1 to $2"
perl -pi -e "s{$1}{$2}g" `find . -name '*.pd'`


echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
