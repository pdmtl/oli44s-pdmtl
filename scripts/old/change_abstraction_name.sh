#bash
############################
# WARNING: This script is dangereous!
# NOTE: For full effect run this script in pdmtl's root folder.
# If you make a mistake, simply make a 'svn revert'
# Finds and replaces all instances of the search string in all the .pd files 
# and replaces them with the replacement string.
# Then it searches for a matching abstraction and help file and changes their names with
# 'svn mv'.
############################


if [ $# -ne 2 ]
then echo Usage : $0 search_string remplacement_string
     exit 1
fi

echo "--- DANGER ZONE ---"
echo "Hope you called this script from the pdmtl root folder!"

for i in `find . -name "$1.pd"` ; do 
echo "Moving $i to $2.pd"
#mv $i $2.pd
done

for i in `find . -name "$1-help.pd"` ; do 
echo "Moving $i to $2-help.pd"
#mv $i $2-help.pd
done

echo "-----"
echo "Replacing all references to $1 with $2"

#perl -pi -e "s{$1}{$2}g" `find . -name '*.pd'`

echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
