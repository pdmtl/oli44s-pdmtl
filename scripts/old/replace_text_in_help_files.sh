#bash
#changes portions of text ONLY in the help files
#cannot change portions of text that have { } characters (this can be solved)

if [ $# -ne 2 ]
then echo Usage : $0 string_searched remplacement_string
     exit 1
fi

echo "--- DANGER ZONE ---"
echo "Hope you called this script from the pdmtl root folder!"


echo "Replacing all references to $1 with $2"

perl -pi -e "s{$1}{$2}g" `find . -name '*-help.pd'`

echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
