#bash
############################
# WARNING: This script is dangereous!
# NOTE: For full effect run this script in pdmtl's root folder.
# If you make a mistake, simply make a 'svn revert'
# Moves the category/abstraction to a new category/abstraction with 'svn_mv'
# Change all references from category/abstraction to category/abstraction
############################



if [ $# -ne 2 ]
then echo Usage : $0 search_string remplacement_string
     exit 1
fi

echo "--- DANGER ZONE ---"
echo "Hope you called this script from the pdmtl root folder!"


HELP_FILE_OLD=$1-help.pd
HELP_FILE_NEW=$2-help.pd
FILE_OLD=$1.pd
FILE_NEW=$2.pd

echo "Moving $FILE_OLD to $FILE_NEW"
svn mv $FILE_OLD $FILE_NEW

echo "Moving $HELP_FILE_OLD to $HELP_FILE_NEW"
svn mv $HELP_FILE_OLD $HELP_FILE_NEW



echo "----"


echo "Changing references from $1 to $2"
perl -pi -e "s{$1}{$2}g" `find . -name '*.pd'`


echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
