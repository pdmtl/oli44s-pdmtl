#bash
############################
# WARNING: This script is dangereous!
# NOTE: For full effect run this script in pdmtl's root folder.
# If you make a mistake, simply make a 'svn revert'
# Moves the .pd & -help.pd files of one category into another category with 'svn_mv'
# Change all references from search_string/* to remplacement_string/*
############################



if [ $# -ne 2 ]
then echo Usage : $0 search_string remplacement_string
     exit 1
fi

ABSTRACTIONS=""

echo "--- DANGER ZONE ---"
echo "Hope you called this script from the pdmtl root folder!"

for i in `find $1 -name "*-help.pd"` ; do 
HELP_FILE_OLD=$i
HELP_FILE_NEW=$2/`basename $i`
FILE_OLD=$1/`basename $i -help.pd`.pd
FILE_NEW=$2/`basename $i -help.pd`.pd

echo "Moving $HELP_FILE_OLD to $HELP_FILE_NEW"
svn mv $HELP_FILE_OLD $HELP_FILE_NEW

echo "Moving $FILE_OLD to $FILE_NEW"
svn mv $FILE_OLD $FILE_NEW

ABSTRACTIONS=$ABSTRACTIONS" `basename $i -help.pd`"
done

echo "----"

for i in $ABSTRACTIONS ; do
OLD_ABSTRACTION=$1/$i
NEW_ABSTRACTION=$2/$i
echo "Changing references from $OLD_ABSTRACTION to $NEW_ABSTRACTION"
perl -pi -e "s{$1}{$2}g" `find . -name '*.pd'`
done

echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
