#bash
############################
# WARNING: This script is dangereous!
# NOTE: For full effect run this script in pdmtl's root folder.
# If you make a mistake, simply make a 'svn revert'

# Converts the folder & _ system to a . system
# svn moves everything, then changes the references in the patches

############################


echo "--- DANGER ZONE ---"
echo "Hope you called this script from the pdmtl root folder!"

CATEGORIES="2d 3d anal convert count data file flow fx gems gui init input list math midi mix musical number random sample scale seq sf synth table timing"
ABSTRACTIONS=""
for i in $CATEGORIES ; do 
ABSTRACTIONS="$ABSTRACTIONS `find $i -name '*.pd'`"
#echo "$i" | sed -e "s/\/\|_/./g"

done

for i in $ABSTRACTIONS ; do 
i=`echo $i | sed -e 's/\/\|_/./g' | sed -e 's/\.\./_./g' | sed -e 's/\.-/_-/g'`
  echo $i
done

#echo $FILES

#for i in `find . -name "$1-help.pd"` ; do 
#echo "Moving $i to `dirname $i`/$2.pd"
#svn mv $i "`dirname $i`/$2-help.pd"
#done

#echo "-----"
#echo "Replacing all references to $1 with $2"

#perl -pi -e "s{$1}{$2}g" `find . -name '*.pd'`

echo "--- DON'T FORGET TO CHECK FOR DAMAGE ---"
echo "--- AND DON'T FORGET TO COMMIT BEFORE RUNNING THIS SCRIPT AGAIN ---"
