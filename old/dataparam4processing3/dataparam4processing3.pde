import processing.opengl.*;

import controlP5.*;
import processing.net.*;




Client net;
ControlP5 controlP5;


String data;
char[] message = new char[600];
int messageLength = 0;
int state = 0;
int xButtons = 0;
int yButtons = 0;
byte semmi = 59;
String tab = "default";
String group;
String name;
char type;
ControllerGroup mouseGroup;



void setup() {
  //size(screen.width, screen.height, OPENGL);
  size(640, 480,OPENGL);
  //size(500,300);
  frameRate(15);
  //startFullscreen(); 
  background(50);
  fill(200);
  net = new Client(this, "127.0.0.1", 9998); // Connect to server on port 80
  controlP5 = new ControlP5(this);
  net.write("groups top;");
  

  controlP5.setColorBackground(color(0,0,0));
  controlP5.setColorForeground(color(160));
  controlP5.setColorValue(color(255,255,255));
  controlP5.setColorLabel(color(255,255,255));
  controlP5.setColorActive(color(255,0,0)); 
}

void draw() {

  background(100);


 if (mouseGroup != null) 
 {
   mouseGroup.setPosition(mouseX+10,mouseY+20);
   
 }

  while (net.available() > 0) { 
    int in = net.read();
    switch (in) {
    case 10: 
      break;
    case 59:
      if (state == 2) parseDump();
      if (state == 1) parseFormat();
      if (state == 0) parseGroups();

      messageLength = 0;
      break;
    default:
      message[messageLength] = char(in);
      messageLength++;
      break;

    } 

    /*
    if (data != null) {
     println(data);
     println("---"); 
     }
     */
  }

}

void mousePressed() {
  if (mouseGroup != null) 
 {
   mouseGroup = null;
 }
}


void controlEvent(ControlEvent theEvent) {
  if ( theEvent.controller().id() > -1) {

    switch (theEvent.controller().id()) {
    case 0:
    
    String buttonLabel = theEvent.controller().label();
    ControllerGroup cGroup = controlP5.group(buttonLabel);
    
    if (cGroup == null) {
      cGroup = controlP5.addGroup(buttonLabel,mouseX+10,mouseY+20);
      mouseGroup = cGroup;
      state =1;
      net.write("format "+buttonLabel+";");
    } else {
      cGroup.remove();
    }
    
    
    //println(theEvent.controller().label());
      
    
      break;
    case 's': 
      net.write(theEvent.controller().name()+" "+theEvent.controller().stringValue()+";");
      break;
    case 'b': 
      net.write(theEvent.controller().name()+" bang;");
      break;
    default:
      net.write(theEvent.controller().name()+" "+theEvent.controller().value()+";");
      break;
    }



  }
}


