void parseDump() {
  String msg = new String(message,0,messageLength);

  if (msg.equals("DONE") ) {
    state = -1;
    println("DONE DUMP"); 
    
  } 
  else {

    String[] things = splitTokens(msg,"@");
    String dumpName = "default";
    for (int i = 0; i < things.length ; i++) {
      String[] subThings = splitTokens(things[i]);
      //println(subThings[0]+" "+subThings[1]);
      if ( subThings[0].equals("id") ) {
        dumpName = subThings[1];

      } 
      else {

        if (controlP5.controller( subThings[0]+"@"+dumpName) != null) {

          if ( controlP5.controller( subThings[0]+"@"+dumpName).id() == 's'  ) {
            ((Textfield) (controlP5.controller(subThings[0]+"@"+dumpName) )).setFocus(true);
            ((Textfield) (controlP5.controller(subThings[0]+"@"+dumpName) )).setValue("rick");
          } 
          else {
            controlP5.controller(subThings[0]+"@"+dumpName).setValue(float(subThings[1]));

          }
        }
      }

    }


  }
}
